import json

import cassiopeia as cass
import gspread
import discord
from discord.ext.commands import Bot, Context

import opgg
from utils import globalize_path
from formatting import (
    in_block,
    format_tooltip,
    format_item_tooltip,
    format_rune_description,
    format_nested_lists_to_table,
)


# Cassiopeia setup
cass.set_riot_api_key(open(globalize_path("./riot_key.txt")).read())
cass.set_default_region("EUW")

# Google sheets setup
gsheet_client = gspread.service_account(filename=globalize_path("./gsheet_key.json"))
sheet = gsheet_client.open("NoobList v. gSheet")


bot = Bot(command_prefix="!", intents=discord.Intents.all())


@bot.command(help="Shows [summoner] rank", usage="[summoner]")
async def summoner_rank(ctx: Context, *args):
    summoner_name = " ".join(args)

    try:
        ranks = cass.get_summoner(name=summoner_name).ranks
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    soloq_rank = ranks.get(cass.Queue.ranked_solo_fives, None)
    soloq_rank = (
        f"{soloq_rank.tier} {soloq_rank.division}" if soloq_rank else "unranked"
    )

    flex_rank = ranks.get(cass.Queue.ranked_flex_fives, None)
    flex_rank = f"{flex_rank.tier} {flex_rank.division}" if flex_rank else "unranked"

    await ctx.send(
        in_block(
            "\n".join(
                (
                    f"# {summoner_name} ranks",
                    f"- **soloQ** : {soloq_rank}",
                    f"- **flexQ** : {flex_rank}",
                )
            )
        )
    )


@bot.command(help="Get [summoner] last games", usage="[summoner]")
async def summoner_last_games(ctx: Context, *args):
    summoner_name = " ".join(args)

    try:
        summoner = opgg.Summoner(summoner_name)
    except Exception:
        await ctx.send("Sorry, I choked while contacting op.gg")
        return

    await ctx.send(in_block("\n".join([f"- {game}" for game in summoner.games])))


@bot.command(help="Shows [item] stats", usage="[item]")
async def item(ctx: Context, *args):
    item_name = " ".join(args)

    try:
        item = cass.get_items().search(item_name)[0]
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    await ctx.send(
        in_block(f"# {item_name}") + in_block(format_item_tooltip(item.description))
    )
    await ctx.send(item.image.url)


@bot.command(help="Shows [champion] stats", usage="[champion]")
async def champ_stats(ctx: Context, *args):
    champion_name = " ".join(args)

    try:
        champion = cass.get_champion(key=champion_name)
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    await ctx.send(
        in_block(f"# {champion_name} stats")
        + in_block(json.dumps(champion.stats.to_dict(), indent=4), language="json")
    )


@bot.command(help="Shows [champion] passive", usage="[champion]")
async def champ_passive(ctx: Context, *args):
    champion_name = " ".join(args)

    try:
        champion = cass.get_champion(key=champion_name)
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    # display passive
    await ctx.send(
        in_block(
            "\n".join(
                (
                    f"# {champion.passive.name}\n",
                    champion.passive.sanitized_description,
                )
            )
        )
    )
    await ctx.send(f"{champion.passive.image_info.url}\n")


@bot.command(help="Shows [champion] [spell key] spell", usage="[champion] [spell key]")
async def champ_spell(ctx: Context, *args):

    if len(args) < 2:
        await ctx.send_help(cspell)
        return

    keyboard_key = args[-1].upper()
    champion_name = " ".join(args[:-1])

    try:
        champion = cass.get_champion(key=champion_name)
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    spells = [s for s in champion.spells if s.keyboard_key.name == keyboard_key]

    # display spells
    for spell in spells:
        cooldowns = "/".join(str(c) for c in spell.cooldowns)
        costs = "/".join(str(c) for c in spell.costs)
        await ctx.send(
            in_block(
                "\n".join(
                    (
                        f"# {spell.name} ({spell.keyboard_key.name})\n",
                        f"*cooldown : {cooldowns}*",
                        f"*costs : {costs}*\n",
                        format_tooltip(spell.tooltip, spell.effects, spell.variables),
                    )
                )
            )
        )
        await ctx.send(spell.image_info.url)


@bot.command(help="Show [champions] cooldowns", usage="[champion1,champion2]")
async def champ_cds(ctx: Context, *args):

    champion_names = " ".join(args).split(",")

    for champion_name in champion_names:

        try:
            champion = cass.get_champion(key=champion_name)
        except Exception:
            await ctx.send("Sorry, I choked while contacting Riot API")
            return

        await ctx.send(
            in_block(
                "\n".join(
                    [f"# {champion_name}"]
                    + [
                        f"- {spell.name} ({spell.keyboard_key.name}) : {spell.cooldowns}"
                        for spell in champion.spells
                    ]
                )
            )
        )


@bot.command(help="Show champion cds for [summoner] current match", usage="summoner")
async def ennemy_cds(ctx: Context, *args):
    summoner_name = " ".join(args)

    try:
        summoner = cass.get_summoner(name=summoner_name)
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    try:
        match = summoner.current_match
    except Exception:
        await ctx.send(f"{summoner_name} is not in game")
        return

    blue_team_names = [
        participant.summoner.name for participant in match.blue_team.participants
    ]
    ennemy_team = (
        match.red_team if summoner_name in blue_team_names else match.blue_team
    )

    # reuse the champ cds command
    await champ_cds(
        ctx,
        ",".join(participant.champion.name for participant in ennemy_team.participants),
    )


@bot.command(help="Show [rune] description", usage="rune")
async def rune(ctx: Context, *args):
    rune_name = " ".join(args)

    try:
        rune = cass.get_runes().find(rune_name)
    except Exception:
        await ctx.send("Sorry, I choked while contacting Riot API")
        return

    await ctx.send(in_block(f"# {rune.name}"))
    await ctx.send(in_block(format_rune_description(rune.long_description)))
    await ctx.send(rune.image.url)


@bot.command(help="Check if Mitsu is in TELETRAVAIL")
async def teletravail(ctx: Context, *args):
    mitsu = [m for m in ctx.guild.members if m.display_name == "Mitsu"][0]
    message = (
        "Mitsu est en télétravail"
        if not mitsu.activity is None and mitsu.activity.name == "Dofus Retro"
        else "Mitsu n'est pas en télétravail"
    )
    await ctx.send(message)


@bot.command(help="Retrieve composition [name] from the GSheet", usage="name")
async def compo_get(ctx: Context, *args):
    comp_name = " ".join(args)
    try:
        worksheet: gspread.Worksheet = sheet.worksheet("%comp " + comp_name)
        await ctx.send(
            in_block(format_nested_lists_to_table(worksheet.get_all_values()))
        )
    except Exception:
        await ctx.send("Sorry, I choked while contacting the GSheet API")


@bot.command(help="Retrieve all composition names from the GSheet")
async def compo_list(ctx: Context, *args):
    prefix = "%comp "
    titles = [
        ws.title[len(prefix) :]
        for ws in sheet.worksheets()
        if ws.title.startswith(prefix)
    ]
    await ctx.send(in_block("\n".join([f"- {title}" for title in titles])))


bot.run(open(globalize_path("./discord_token.txt")).read().strip())
