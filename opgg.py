from __future__ import annotations
from typing import Tuple, List
import re

import requests
from bs4 import BeautifulSoup


def request_summoner(name: str) -> requests.Response:
    encoded_name = name.replace(" ", "+")
    return requests.get(f"https://euw.op.gg/summoner/userName={encoded_name}")


class Game:
    def __init__(self, kda: Tuple[int, int, int], result: str) -> None:
        self.kda = kda
        self.result = result

    @classmethod
    def games_from_page(cls, content: str) -> List[Game]:

        games = []
        soup = BeautifulSoup(content, features="lxml")

        last_games_div = soup.find_all("div", "GameItemWrap")

        for game_div in last_games_div:

            kda_div = game_div.find("div", "KDA")
            kills_nb = int(kda_div.find("span", "Kill").text)
            deaths_nb = int(kda_div.find("span", "Death").text)
            assists_nb = int(kda_div.find("span", "Assist").text)
            kda = (kills_nb, deaths_nb, assists_nb)

            result = game_div.find("div", re.compile(r"^GameItem.*$")).attrs[
                "data-game-result"
            ]

            games.append(Game(kda, result))

        return games

    def __str__(self) -> str:
        return f"{self.result} {str(self.kda)}"


class Summoner:
    def __init__(self, name: str) -> None:

        ans = request_summoner(name)

        if ans.status_code != 200:
            raise Exception

        self.games = Game.games_from_page(ans.text)
