import re
from typing import List


def in_block(block: str, language: str = "markdown") -> str:
    return f"```{language}\n{block}\n```"


def format_rune_description(desc: str) -> str:
    desc = re.sub(r"<br>", "\n", desc)
    desc = re.sub(r"<\/?i>", "_", desc)
    desc = re.sub(r"<.*?>", "", desc)
    return desc


def format_item_tooltip(tooltip: str) -> str:
    tooltip = re.sub(r"<br>", "\n", tooltip)
    tooltip = re.sub(r"<.*?>", "", tooltip)
    return tooltip


def format_tooltip(tooltip: str, effects: list, variables):
    tooltip = re.sub(r"<br \/><br \/>", r"\n", tooltip)
    tooltip = re.sub(r"<\/?keywordMajor>", r"**", tooltip)
    tooltip = re.sub(r"<\/?magicDamage>", r"*", tooltip)
    tooltip = re.sub(r"<\/?physicalDamage>", r"*", tooltip)

    # {{ eN }} placeholders
    tooltip = re.sub(
        r"\{\{\ e([0-9]+) }\}",
        lambda m: "/".join(map(str, effects[int(m.group(1))])),
        tooltip,
    )

    # {{ aN }} and {{ fN placeholders }}
    def replace_advanced_placeholders(placeholder) -> str:
        try:
            var = variables.search(placeholder)[0]
            return "/".join(map(str, var.coefficients))
        except Exception:
            return placeholder

    tooltip = re.sub(
        r"\{\{\ (a[0-9]+|f[0-9]+) }\}",
        lambda m: replace_advanced_placeholders(m.group(1)),
        tooltip,
    )

    tooltip = re.sub(r"<\/?.*?>", r"", tooltip)

    return tooltip


def format_nested_lists_to_table(
    nested_lists: List[List[str]], max_line_len: int = 80
) -> str:

    column_sizes = [
        max([len(cell) for cell in column]) for column in zip(*nested_lists)
    ]

    lines = [[] for _ in nested_lists]

    # lines creation
    for line_list, line in zip(nested_lists, lines):
        line.append("| ")
        for column_idx, cell in enumerate(line_list):
            line += [cell, " " * (column_sizes[column_idx] - len(cell)), " | "]
        line.append("\n")

    # lines padding
    lines = ["".join(line) for line in lines]
    for idx, line in enumerate(lines):
        if len(line) > max_line_len:
            lines[idx] = line[: max_line_len - 4] + "..." + "\n"

    # separator creation
    separator = ["+ "]
    for column_size in column_sizes:
        separator.append("-" * column_size)
        separator.append(" + ")
    separator = "".join(separator)[: max_line_len - 4] + "..." + "\n"

    if len(lines) <= 1:
        return "".join(lines)
    return "".join([lines[0]] + [separator] + lines[1:])
